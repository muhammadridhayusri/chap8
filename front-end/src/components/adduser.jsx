import React,{useState, useEffect} from 'react'
import axios from "axios"
import { useNavigate } from "react-router-dom"


function AddUser() {
  const [username, setUsername] = useState("")
  const [email, setEmail] = useState("")
  const [password, setPassword] = useState("")
  const [experience, setExperience] = useState(0)
  const navigate = useNavigate()
  
  const saveUser = async (e) => {
    try {
      e.preventDefault()
      await axios.post("http://localhost:4000/api/v1/players",{
        username,
        email,
        password,
        experience
    })
    navigate('/');
    console.log(`add has been executed`)
    } catch (error) {
      console.log(error + "cause of double id or password < 6 digit")
    }
  }

  return (
    <form onSubmit={saveUser}>
  <div className="mb-3">
    <label className="form-label">Username</label>
    <input 
      type="username" 
      className="form-control" 
      value={username} 
      onChange={(e) => setUsername(e.target.value)}
    />

  </div>
  <div className="mb-3">
    <label className="form-label">Email address</label>
    <input 
      type="email" 
      className="form-control" 
      value={email} 
      onChange={(e) => setEmail(e.target.value)}
    />

  </div>
  <div className="mb-3">
    <label className="form-label">Password</label>
    <input 
      type="password" 
      className="form-control" 
      value={password} 
      onChange={(e) => setPassword(e.target.value)}
    />
  </div>
  <div className="mb-3">
    <label className="form-label">Experience</label>
    <input 
      type="number" 
      className="form-control" 
      value={experience} 
      onChange={(e) => setExperience(e.target.value)}
    />
  </div>
  <button type="submit" className="btn btn-primary">Submit</button>
</form>
  )
}

export default AddUser